package ro.orangetraining;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        Map<String, Integer> hmap = new HashMap();
        //Adding elements to HashMap
        hmap.put("Bartenders",5);
        hmap.put("Receptionist",3);
        hmap.put("Cashiers",10);
        hmap.put("Porters",8);

        System.out.println("Total type of hotel employees: " +hmap.size());

        // varianta 1
       /*Iterator iterator = hmap.entrySet().iterator();
        while(iterator.hasNext()) {
           Map.Entry mentry = (Map.Entry)iterator.next();
           System.out.println(mentry.getValue() +" - "+ mentry.getKey());
                 }*/


        // Varianta 2
        for (String altceva : hmap.keySet()) {
            System.out.println(altceva +" - " +hmap.get(altceva));
            //System.out.println(hmap.get(altceva));
        }

        //Found total 10 Cashiers employees! ????
        String searchKey = "Cashiers";
        if(hmap.containsKey(searchKey))
            System.out.println("Found total " + hmap.get(searchKey) +" " +searchKey + " employees!");


        hmap.clear();
        System.out.println("After clear operation, size is now: " +hmap.size());

    }
}
